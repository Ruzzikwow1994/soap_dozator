/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define UV_LED_Pin GPIO_PIN_15
#define UV_LED_GPIO_Port GPIOC
#define LIMIT_SWITCH_Pin GPIO_PIN_2
#define LIMIT_SWITCH_GPIO_Port GPIOA
#define LIMIT_SWITCH_EXTI_IRQn EXTI2_3_IRQn
#define MOTOR_EN_Pin GPIO_PIN_3
#define MOTOR_EN_GPIO_Port GPIOA
#define MOTOR_HALT_Pin GPIO_PIN_4
#define MOTOR_HALT_GPIO_Port GPIOA
#define LED_STATE_A_Pin GPIO_PIN_11
#define LED_STATE_A_GPIO_Port GPIOA
#define LED_STATE_K_Pin GPIO_PIN_12
#define LED_STATE_K_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */
#define LED_GREEN HAL_GPIO_WritePin(LED_STATE_K_GPIO_Port, LED_STATE_K_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(LED_STATE_A_GPIO_Port, LED_STATE_A_Pin, GPIO_PIN_SET); green_on=1
#define LED_RED HAL_GPIO_WritePin(LED_STATE_A_GPIO_Port, LED_STATE_A_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(LED_STATE_K_GPIO_Port, LED_STATE_K_Pin, GPIO_PIN_SET); red_on=1
#define LED_OFF HAL_GPIO_WritePin(LED_STATE_K_GPIO_Port, LED_STATE_K_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(LED_STATE_A_GPIO_Port, LED_STATE_A_Pin, GPIO_PIN_RESET); green_on=0; red_on=0

#define LED_UV_ON HAL_GPIO_WritePin(UV_LED_GPIO_Port,UV_LED_Pin,GPIO_PIN_SET)
#define LED_UV_OFF HAL_GPIO_WritePin(UV_LED_GPIO_Port,UV_LED_Pin,GPIO_PIN_RESET)

#define MOTOR_ON  HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_RESET);for(int i=0; i<1000; i++){}; HAL_GPIO_WritePin(MOTOR_EN_GPIO_Port,MOTOR_EN_Pin,GPIO_PIN_SET); LED_UV_ON; motor_state=1
#define MOTOR_OFF HAL_GPIO_WritePin(MOTOR_EN_GPIO_Port,MOTOR_EN_Pin,GPIO_PIN_RESET)//;for(int i=0; i<1000; i++){}; HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_SET); LED_UV_OFF; motor_state=0

typedef enum 
{
	GREEN,
	RED
} led_color;

struct Flags{
	uint8_t  opto_sensor;
	uint8_t  motor_sw;
	uint8_t	 motor_timer;

};
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
