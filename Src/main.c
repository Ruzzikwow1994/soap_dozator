/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "rtc.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

enum states
{
	s0, //ожидание/сон
	s1, //прокачка штатная зеленый мигает
	s2,	//прокачка кончилась оптопара замкнута красный мигает черз 0.5сек
	s3, //прокачка шла более 3сек сломался концевик авария
	
};

enum signals 
{			//скт     //сенсор концевик таймер
	x0, //000
	x1, //001
	x2, //010
	x3, //011
	x4, //100
	x5, //101
	x6, //110
	x7  //111
};

enum signals sig_table[8] = 
{
	[0] = x0,
	[1] = x1,
	[2] = x2,
	[3] = x3,
	[4] = x4,
	[5] = x5,
	[6] = x6,
	[7] = x7
};

enum states FSM_TABLE[4][8] = {
	[s0][x0] = s0,	[s1][x0] = s1,	[s2][x0] = s0,	[s3][x0] = s3,	
	[s0][x1] = s0,	[s1][x1] = s3,	[s2][x1] = s0,	[s3][x1] = s3,	
	[s0][x2] = s0,	[s1][x2] = s0,	[s2][x2] = s0,	[s3][x2] = s3,	
	[s0][x3] = s0,	[s1][x3] = s3,	[s2][x3] = s0,	[s3][x3] = s3,	
	[s0][x4] = s1,	[s1][x4] = s1,	[s2][x4] = s2,	[s3][x4] = s3,	
	[s0][x5] = s1,	[s1][x5] = s3,	[s2][x5] = s2,	[s3][x5] = s3,	
	[s0][x6] = s1,	[s1][x6] = s2,	[s2][x6] = s2,	[s3][x6] = s3,	
	[s0][x7] = s1,	[s1][x7] = s3,	[s2][x7] = s2,	[s3][x7] = s3,
};

uint8_t recieveChar;
uint8_t transmitChar=0x55;
uint8_t working_flag;
int blink_cnt;
uint8_t red_flag;
uint8_t green_flag;
uint8_t green_on;
uint8_t red_on;
uint8_t motor_enable;
uint8_t motor_state;
uint8_t opto_sensor;
uint8_t first_time=1;
uint8_t error;
int motor_cnt;
int opto_ctrl;
struct Flags flags={0};
int red_cnt=10;
int cnt_work;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void doFSM_table(void);
enum signals getSignal(void);
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */
	HAL_UART_Receive_IT(&huart1,&recieveChar,1);
	
  /* USER CODE END 2 */
	if(HAL_GPIO_ReadPin(LIMIT_SWITCH_GPIO_Port,LIMIT_SWITCH_Pin) == GPIO_PIN_SET)
	{
		flags.opto_sensor=1;
	}
	else
	{
		HAL_Delay(50);
	}
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		
		doFSM_table();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage 
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks 
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_SYSTICK_Callback(void)
{

	
	if(flags.opto_sensor) //счетчик от залипания в красном режиме когда нет отражения и фалг оптосенсора не снимается
	{
		opto_ctrl++;
	}
	else
	{
		opto_ctrl=0;
	}
	if(opto_ctrl>500)
	{
		opto_ctrl=0;
		flags.opto_sensor=0;
	}
	
	HAL_UART_Transmit_IT (&huart1,&transmitChar,1); //11 байт в секунду должен успевать передать
	blink_cnt++;
	if(motor_state)
	{
		motor_cnt++;
	}
	else
	{
		motor_cnt=0;
	}
	
	if(motor_cnt>3000)
	{	
		flags.motor_timer=1;
	}
	if(blink_cnt>100) //таймер для милагки
	{
		
		blink_cnt=0;
		if(green_flag)
		{
			if(green_on) //если уже горит зеленый
			{
				LED_OFF; //выключить
			}
			else
			{
				LED_GREEN; //иначе включить
			}
		}
		else if(red_flag)
		{
			if(red_cnt>0)
			{
			red_cnt--;
			LED_OFF;
			}
			if(red_cnt==0)
			{
				if(red_on)
				{
					LED_OFF;
				}
				else
				{
					LED_RED;
				}
			}
		}
		else
		{
			red_cnt=10;
			LED_OFF;
		}
	}
	
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	opto_ctrl=0; //пока есть прерывания есть отражение счетчик не нужен
	if(recieveChar==transmitChar)
	{
		working_flag=1; //чтобы не уснуть
		//работаем
		
		opto_sensor=1; //для проверки выключения	
		
		flags.opto_sensor=1;
		
	}
	else
	{
		opto_sensor=0; 
		flags.opto_sensor=0;
		//ложное
	}
	HAL_UART_Receive_IT(&huart1,&recieveChar,1);
}





void doFSM_table()
{
    enum states current_state = s0;
    while (1)
    {
			
	
        enum signals current_signal = getSignal();
        current_state = FSM_TABLE[current_state][current_signal];

				switch(current_state)
				{
					case s0:
						

						if(motor_state)
						{
						HAL_GPIO_WritePin(MOTOR_EN_GPIO_Port,MOTOR_EN_Pin,GPIO_PIN_RESET);
						HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_SET);
						LED_UV_OFF;
						HAL_Delay(500);
						motor_state=0;
						}
						if(motor_cnt)
						motor_cnt=0;
						if(green_flag)
						green_flag=0;
						if(red_flag)
						red_flag=0;
						flags.motor_sw=0;
						HAL_SuspendTick();
			
						HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI); // PWR_MAINREGULATOR_ON
						// после wakeup'а программа стартует отсюда
						HAL_ResumeTick();
						SystemClock_Config(); // рестартуем системный клок
						
						HAL_NVIC_SystemReset();
						break;
					case s1:
					
						if(!motor_state)
						{
							HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_RESET);
							HAL_GPIO_WritePin(MOTOR_EN_GPIO_Port,MOTOR_EN_Pin,GPIO_PIN_SET);
							LED_UV_ON;
							motor_state=1;
						}
						if(!green_flag)
						green_flag=1;
						if(red_flag)
						red_flag=0;


						break;
					case s2:
						
						if(motor_state)
						{
						HAL_GPIO_WritePin(MOTOR_EN_GPIO_Port,MOTOR_EN_Pin,GPIO_PIN_RESET);
						HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_SET);
						LED_UV_OFF;
						motor_state=0;
						}
						if(green_flag)
						green_flag=0;
						if(!red_flag)
						red_flag=1;
				
						break;
					case s3:
						
						if(motor_state)
						{
						HAL_GPIO_WritePin(MOTOR_EN_GPIO_Port,MOTOR_EN_Pin,GPIO_PIN_RESET);
						HAL_GPIO_WritePin(MOTOR_HALT_GPIO_Port,MOTOR_HALT_Pin,GPIO_PIN_SET);
						LED_UV_OFF;
						motor_state=0;
						}
						if(green_flag)
						green_flag=0;
						if(!red_flag)
						red_flag=1;

						break;
				}
		}
}

enum signals getSignal(void)
{
	uint8_t num_state=0;
	if(flags.opto_sensor)
	{
		num_state+=4;
	}
	if(flags.motor_timer)
	{
		num_state++;
	}
	if(flags.motor_sw)
	{
		num_state+=2;
	}

	
	enum signals getted_sig = sig_table[num_state];
	return getted_sig;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
